import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './page/login/login.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



import { environment } from "../environments/environment";
import { AngularFireModule } from "@angular/fire"
import { AngularFireStorageModule, BUCKET } from '@angular/fire/storage';
import { AngularFireAuthGuardModule } from '@angular/fire/auth-guard';
import { HomeComponent } from './page/home/home.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { ImageCropperModule } from 'ngx-image-cropper';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { RegistationComponent } from './page/registation/registation.component';
import { LoadingComponent } from './widget/loading/loading.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { PostViewComponent } from './page/post-view/post-view.component';
import { UserProfileComponent } from './page/user-profile/user-profile.component';
import { EditProfileComponent } from './page/edit-profile/edit-profile.component';
import { PostUploderComponent } from './page/post-uploder/post-uploder.component';
import { UserPostsComponent } from './widget/user-posts/user-posts.component';
import { ImageViewComponent } from './widget/image-view/image-view.component';
import { UserViewComponent } from './page/user-view/user-view.component';
import { UserSearchComponent } from './page/user-search/user-search.component';
import { LazyloadDirective } from './directive/lazyload/lazyload.directive';
import { AlertMsgComponent } from './widget/alert-msg/alert-msg/alert-msg.component';
import { ForgotpasswordComponent } from './widget/pass/forgotpassword/forgotpassword.component';
import { OneColumnPostComponent } from './widget/one-column-post/one-column-post.component';
@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        RegistationComponent,
        LoadingComponent,
        PostViewComponent,
        UserProfileComponent,
        EditProfileComponent,
        PostUploderComponent,
        UserPostsComponent,
        ImageViewComponent,
        UserViewComponent,
        UserSearchComponent,
        LazyloadDirective,
        AlertMsgComponent,
        ForgotpasswordComponent,
        OneColumnPostComponent,

    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        ImageCropperModule,

        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireAuthGuardModule,
        NgxSpinnerModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
        NgxMatFileInputModule,
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatStepperModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTreeModule,


    ],
    providers: [{ provide: BUCKET, useValue: 'gs://fir-project-ef30c.appspot.com/' }],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
