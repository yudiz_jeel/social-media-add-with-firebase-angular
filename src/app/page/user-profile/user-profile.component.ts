import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/provider/auth/auth.service';
import { DbmanagerService } from 'src/app/provider/dbmanager/dbmanager.service';
import { MatDialog } from '@angular/material/dialog';
import { ImageViewComponent } from 'src/app/widget/image-view/image-view.component';
import { MassageService } from 'src/app/provider/msg/massage.service';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

    constructor(private _dbservice: DbmanagerService,
        private _authservice: AuthService,
        private dialog: MatDialog,
        private _msg: MassageService
    ) {

    }
    userPosts: any[] = []
    userdata
    ngOnInit(): void {
        this._dbservice.userData.subscribe(data => {
            console.log(data);
            this.userdata = data
            this._dbservice.getUserPosts().subscribe(data => {
                this.userPosts = data
            })
        })

    }

    logout() {
        this._msg.presentAlertDilog("Are you sure, want to Logout").afterClosed().subscribe(data=>{
            if(data){
                this._authservice.logout()
            }
        })

    }


}
