import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/provider/auth/auth.service';
import { DbmanagerService } from 'src/app/provider/dbmanager/dbmanager.service';
import { MassageService } from 'src/app/provider/msg/massage.service';

@Component({
    selector: 'app-registation',
    templateUrl: './registation.component.html',
    styleUrls: ['./registation.component.scss']
})
export class RegistationComponent implements OnInit {
    public SignupForm: FormGroup
    public loader: boolean = false
    constructor(private fb: FormBuilder,
        private _authservice: AuthService,
        private _msgservice: MassageService,
        private _dbservice: DbmanagerService,
        private router: Router) { }

    ngOnInit(): void {
        this.SignupForm = this.fb.group({
            name: ['', Validators.required],
            email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+[.][a-zA-Z0-9.-]+$')]],
            password: ['', Validators.required],
            copass: ['', Validators.required],
            longitude: [null],
            latitude: [null],
            term: ['', Validators.requiredTrue]


        }, { validator: this.customvalidator() })

        this.SignupForm.valueChanges.subscribe(data => {
            console.log(data);

        })
        

    }

    customvalidator(): ValidatorFn {
        return (control: AbstractControl) => {
            if (control.get('password').value != control.get('copass').value) {
                return { 'notMatch': true }
            }
            return null
        }
    }

    get Email() {
        return this.SignupForm.get('email') as FormControl
    }


    get Copass() {
        return this.SignupForm.get('copass')
    }
    get latitude() {
        return this.SignupForm.get('latitude')
    }
    get longitude() {
        return this.SignupForm.get('longitude')
    }


    async register() {
        console.log(this.SignupForm.value);
        if (this.SignupForm.invalid) {
            this._msgservice.openSnackBar("Please fill inputs")
        } else {
            this.loader = true
            await this.getPosition()
            this._authservice.Registation(this.SignupForm.value).then(async data => {
                data.user.uid
                const userData = this.SignupForm.value
                userData.id = data.user.uid
                delete userData.password
                delete userData.copass
                delete userData.term
                this._dbservice.storeNewUser(userData, data.user.uid).then(data => {
                    this.loader = false
                    this.router.navigateByUrl("/home")
                })
            })
        }
        //this.user.Registion(this.SignupForm.value)
    }

    getPosition() {
        let options = {
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 0
        };

        let regi = navigator.geolocation.watchPosition(resp => {
            //console.log(resp.coords.longitude);
            //console.log(resp.coords.latitude);
            this.latitude.setValue(resp.coords.latitude)
            this.longitude.setValue(resp.coords.longitude)
            navigator.geolocation.clearWatch(regi);
            return { lng: resp.coords.latitude, long: resp.coords.longitude }
        }, null, options)
    }



}
