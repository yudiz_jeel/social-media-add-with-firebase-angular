import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-user-view',
    templateUrl: './user-view.component.html',
    styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {

    constructor(private route: ActivatedRoute, private location: Location) { }
    data
    user: Observable<any>
    posts: Observable<any>
    ngOnInit(): void {
        this.user = this.route.snapshot.data['user'].user;
        this.posts = this.route.snapshot.data['user'].posts;
        this.user.subscribe(console.log);
        this.posts.subscribe(console.log);
    }

    back() {
        this.location.back()
    }

}
