import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { finalize, tap } from 'rxjs/operators';
import { DbmanagerService } from 'src/app/provider/dbmanager/dbmanager.service';
import { MassageService } from 'src/app/provider/msg/massage.service';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {


    editForm: FormGroup
    currentUser
    userID = localStorage.getItem('id')
    loader = false

    constructor(
        private fb: FormBuilder,
        private location: Location,
        private _dbmanger: DbmanagerService,
        private storage: AngularFireStorage,
        private _msgService: MassageService
    ) {
        this._dbmanger.userData.subscribe(data => {
            this.currentUser = data
            console.log(this.editForm.value);

        })

    }

    ngOnInit(): void {
        this.editForm = this.fb.group({
            name: [this.currentUser?.name],
            email: [this.currentUser?.email],
            dob: [this.currentUser?.dob],
            number: [this.currentUser?.number],
            propic: [this.currentUser?.propic],
            bio: [this.currentUser?.bio]
        })
        this.editForm.valueChanges.subscribe(console.log)
        setTimeout(() => {
            this.editForm.get("name").setValue(this.currentUser?.name)
            this.editForm.get("email").setValue(this.currentUser?.email)
            this.editForm.get("dob").setValue(this.currentUser?.dob)
            this.editForm.get("bio").setValue(this.currentUser?.bio)
            this.editForm.get("number").setValue(this.currentUser?.number)
            this.editForm.get("propic").setValue(this.currentUser?.propic)
        }, 500);

    }

    back() {
        this.location.back()
    }

    get Propic() {
        return this.editForm.get('propic') as FormControl
    }

    get Email() {
        return this.editForm.get('email') as FormControl
    }

    changeImage(ev) {
        this.loader = true
        console.log(ev);
        const file = ev.target.files[0];
        console.log(`users/profile/${this.userID}/${file.name}`);

        const filePath = `users/profile/${this.userID}/${file.name}`;
        const ref = this.storage.ref(filePath);
        const task = ref.put(file);

        task.snapshotChanges().pipe(
            finalize(() => ref.getDownloadURL().subscribe(data => { this.Propic.setValue(data) })),
            tap(() => { this.loader = false; })
        ).subscribe()
    }


    updateData() {
        this._dbmanger.updateProfileData(this.editForm.value).then(data => {
            this._msgService.openSnackBar("Profile Update succefully")
        })
    }

}
