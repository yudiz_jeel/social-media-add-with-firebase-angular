import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//import { } from '@types/googlemaps';
@Component({
    selector: 'app-user-location',
    templateUrl: './user-location.component.html',
    styleUrls: ['./user-location.component.scss']
})
export class UserLocationComponent implements OnInit {
    @ViewChild('gmap') gmapElement: any;
    map: google.maps.Map;
    constructor(private routre: ActivatedRoute) {
    }

    lat;
    long
    marker: google.maps.Marker
    ngOnInit(): void {
        //const { lat, long } = 
        this.routre.queryParams.subscribe(params => {
            this.lat = params["lat"]
            this.long = params["long"]
        })
        console.log(this.lat, this.long);

        var mapProp = {
            center: new google.maps.LatLng(this.lat, this.long),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,

        };
        setTimeout(() => {
            this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
            let location = new google.maps.LatLng(this.lat, this.long)
            this.map.panTo(location);
            if (!this.marker) {
                this.marker = new google.maps.Marker({
                    position: location,
                    map: this.map,
                });
            }
            else {
                this.marker.setPosition(location);
            }
        }, 500);
    }
}


