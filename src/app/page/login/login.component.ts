import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AuthService } from 'src/app/provider/auth/auth.service';
import { MassageService } from 'src/app/provider/msg/massage.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserprofileService } from 'src/app/provider/userprofile/userprofile.service';
import { DbmanagerService } from 'src/app/provider/dbmanager/dbmanager.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup
    loader: boolean = false
    constructor(
        private router: Router,
        private _authService: AuthService,
        private fb: FormBuilder,
        private _msgService: MassageService,
        private _userProfie: UserprofileService,
        private _dbmanger: DbmanagerService,
    

    ) {

    }

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        })

    }


    loginWithEmail() {
        if (!this.loginForm.invalid) {
            this.loader = true
            this._authService.LoginWithEmail(this.loginForm.value)
                .then(res => {
                    console.log(res.user.uid);
                    localStorage.setItem("id", res.user.uid)
                    this._userProfie.userUID.next(res.user.uid)
                    this.getPosition()
                    this.loader = false
                    this.router.navigateByUrl("/home")

                }).catch(err => {
                    this.loader = false
                    this._msgService.openSnackBar(err.message)
                })
        } else {
            this._msgService.openSnackBar("Please fill inputs")
        }
    }

    loginWithGoogle() {
        this._authService.LoginWithGoogle().then(async data => {
            console.log(data.user.uid);
            localStorage.setItem("id", data.user.uid)
            this.getPosition()
            this._userProfie.userUID.next(data.user.uid)
            this.router.navigateByUrl("/home")
        }).catch(err => {
            this._msgService.openSnackBar(err.message)
        })
    }
    openForgot() {
        this._msgService.presentForgotpassDilog()
    }
    get email() {
        return this.loginForm.get("email")
    }

    get password() {
        return this.loginForm.get("password")
    }

    getPosition() {
        let options = {
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 0
        };

        let regi = navigator.geolocation.watchPosition(resp => {
            this._dbmanger.updateLatLng({ latitude: resp.coords.latitude, longitude: resp.coords.longitude })
            console.log({ latitude: resp.coords.latitude, longitude: resp.coords.longitude });

            navigator.geolocation.clearWatch(regi);
            return { latitude: resp.coords.latitude, longitude: resp.coords.longitude }
        }, null, options)
    }

}
