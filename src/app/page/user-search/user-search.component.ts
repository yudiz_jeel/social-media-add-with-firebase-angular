import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, pluck, startWith, tap } from 'rxjs/operators';
import { DbmanagerService } from 'src/app/provider/dbmanager/dbmanager.service';

@Component({
    selector: 'app-user-search',
    templateUrl: './user-search.component.html',
    styleUrls: ['./user-search.component.scss']
})
export class UserSearchComponent implements OnInit {

    search = new FormControl();
    constructor(private _dbmannager: DbmanagerService) { }
    usersdata: Observable<any[]>
    filterData: Observable<any[]>
    ngOnInit(): void {
        this.search.valueChanges.pipe(debounceTime(200), distinctUntilChanged()
            , tap(console.log),
            map(ele => ele ? this.filterByName(ele) : "")).subscribe((data: any) => {
                this.filterData = data
                //console.log(data);

            })


    }

    filterByName(name) {
        return this._dbmannager.getUserCollection().pipe(map((data: any) => {
            return data.filter(ele => {
                return ele.name.toLowerCase().startsWith(name.toLowerCase())
            })
        }), tap(console.log))
    }

}
