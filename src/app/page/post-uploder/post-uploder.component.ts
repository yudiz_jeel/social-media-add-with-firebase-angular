import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { DbmanagerService } from 'src/app/provider/dbmanager/dbmanager.service';
import { MassageService } from 'src/app/provider/msg/massage.service';
import { UserprofileService } from 'src/app/provider/userprofile/userprofile.service';

@Component({
    selector: 'app-post-uploder',
    templateUrl: './post-uploder.component.html',
    styleUrls: ['./post-uploder.component.scss']
})
export class PostUploderComponent implements OnInit {
    postForm: FormGroup
    data = []
    constructor(private fb: FormBuilder,
        private storage: AngularFireStorage,
        private _dbMan: DbmanagerService,
        private _user: UserprofileService,
        private _msgservice: MassageService) {
        _dbMan.getUserCollection().subscribe(data => {
            this.data = data
        })
    }

    ngOnInit(): void {
        this.postForm = this.fb.group({
            id: "",
            userId: this._user.userUID.value,
            title: ["", Validators.required],
            desc: "",
            createat: "",
            file: ["", Validators.required],
            imgsrc: "",
        })

        this.postForm.valueChanges.subscribe(console.log)
        this.File.valueChanges.subscribe(data => {
            console.log(data);
            this.imagecroper = data

        })
    }
    get File() {
        return this.postForm.get('file') as FormControl
    }
    get imgsrc() {
        return this.postForm.get('imgsrc') as FormControl
    }
    get createat() {
        return this.postForm.get("createat") as FormControl
    }
    get id() {
        return this.postForm.get("id") as FormControl
    }

    CropImage;
    imageCropped(ev) {
        console.log(ev);
        this.CropImage = ev.base64
        //log
    }
    imagecroper = false
    uploadPercent
    upload() {
        const file = this.File.value
        const filePath = "posts/" + file.name
        console.log(filePath);
        const ref = this.storage.ref(filePath)
        const task = ref.putString(this.CropImage, "data_url")
        this.uploadPercent = task.percentageChanges();

        let date = new Date()
        this.createat.setValue(date)
        this.id.setValue(date.getTime().toString())


        task.snapshotChanges().pipe(
            finalize(() => ref.getDownloadURL().subscribe(data => {
                this.File.setValue(data)
                this.imgsrc.setValue(filePath)
                this.upload_data()
                this.imagecroper = false
                this._msgservice.openSnackBar("Post upload successfully")
            }))
        )
            .subscribe()

    }

    upload_data() {
        this._dbMan.uploaduserPostData(this.postForm.value).then(() => {
            this.imagecroper = false
            this._msgservice.openSnackBar("Post upload successfully")
            this.postForm.reset()
            this.postForm.untouched
        })
    }

}
