import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostUploderComponent } from './post-uploder.component';

describe('PostUploderComponent', () => {
  let component: PostUploderComponent;
  let fixture: ComponentFixture<PostUploderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostUploderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostUploderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
