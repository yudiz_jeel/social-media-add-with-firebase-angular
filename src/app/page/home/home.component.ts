import { Platform } from '@angular/cdk/platform';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { fromEvent, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, tap, } from 'rxjs/operators';
import { DbmanagerService } from 'src/app/provider/dbmanager/dbmanager.service';
import { UserprofileService } from 'src/app/provider/userprofile/userprofile.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    PostData: Observable<any>
    fragment = "home"
    select = 0
    isDesktop: boolean = false
    isMobilep: boolean = false
    isError: boolean = false
    _resize$
    constructor(private _userProfile: UserprofileService,
        private _dbmanager: DbmanagerService,
        private router: ActivatedRoute,
        private platform: Platform
    ) {
        router.fragment.subscribe(data => {
            this.fragment = data || 'home'
            this.select = this.setIndex(data)
        })
        //this._resize$ = fromEvent(window, 'resize')
        //    .pipe(
        //        debounceTime(200),
        //        map(() => window.innerWidth),
        //        distinctUntilChanged(),
        //        startWith(window.innerWidth),
        //        tap(width => this.setWindowWidth(width)),
        //    );
        //this._resize$.subscribe();

        //if (!platform.ANDROID && !platform.IOS) {
        //    this.isDesktop = true
        //} else {
        //    this.isMobilep = true
        //}


        //console.log(this.isDesktop, this.isMobilep);


        this.isError = this._userProfile.userUID.toPromise() ? false : true;
        this.PostData = this._dbmanager.postCollection$.valueChanges()
    }

    ngOnInit(): void {
    }
    index = 0
    getLoadedComponent(index) {
        this.index = index
    }

    log(ev) {
        console.log(ev);

    }
    onClick() {
        console.log("click");

    }

    setIndex(data): number {
        if (data == 'home') {
            return 0
        } else if (data == 'search') {
            return 1
        } else if (data == 'newpost') {
            return 2
        } else if (data == 'profile') {
            return 3
        } else {
            return 0
        }
    }
    isTablete
    setWindowWidth(width) {
        console.log(width);

        if (width <= 600) {
            this.isMobilep = true
            this.isDesktop = false
            this.isTablete = false

        } else if (width > 600 && width <= 767) {
            this.isMobilep = false
            this.isTablete = true
            this.isDesktop = false

        } else if (width > 767 && width <= 1280) {
            this.isMobilep = false
            this.isTablete = false
            this.isDesktop = true
        }

        console.log({ Desktop: this.isDesktop }, { Mobile: this.isMobilep }, { Tablete: this.isTablete });


    }
}
