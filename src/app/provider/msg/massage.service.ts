import { Component, Inject, Injectable } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertMsgComponent } from 'src/app/widget/alert-msg/alert-msg/alert-msg.component';
import { ForgotpasswordComponent } from 'src/app/widget/pass/forgotpassword/forgotpassword.component';

@Injectable({
    providedIn: 'root'
})
export class MassageService {

    constructor(private _snackBar: MatSnackBar,
        private spinner: NgxSpinnerService,
        public dialog: MatDialog) {



    }

    ngOnInit() {
        this.spinner.show();
        setTimeout(() => {
            this.spinner.hide();
        }, 2000);
    }

    showSpinner() {
        this.spinner.show();
        setTimeout(() => {
            this.spinner.hide();
        }, 3000);
    }

    openSnackBar(message: string) {
        this._snackBar.open(message, "OK", {
            duration: 2000,
            verticalPosition: 'top'
        });

    }

    presentAlertDilog(msg) {
        return this.dialog.open(AlertMsgComponent, { data: { msg: msg } });
    }
    presentForgotpassDilog() {
        return this.dialog.open(ForgotpasswordComponent);
    }



}
//@Component({
//    selector: 'dialog-content-alert-dialog',
//    templateUrl: 'dialog-content-alert-dialog.html',
//})
//export class DialogContentAlertDialog {
//    constructor(public dialogRef: MatDialogRef<DialogContentAlertDialog>,
//        @Inject(MAT_DIALOG_DATA) public data) { }

//    onNoClick(): void {
//        this.dialogRef.close();
//    }
//}