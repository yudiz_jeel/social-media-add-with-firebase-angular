import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SingleUserService implements Resolve<any>{

    constructor(private afs: AngularFirestore) { }
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        const id = route.paramMap.get('id')
        return { user: this.getUser(id), posts: this.getPost(id) };
    }



    getUser(id) {
        return this.afs.collection("users").doc(id).valueChanges()
    }
    getPost(id) {
        return this.afs.collection("posts", ref => ref.where("userId", '==', id).orderBy("id", "desc")).valueChanges()
    }
}
