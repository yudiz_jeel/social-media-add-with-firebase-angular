import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserprofileService {
    public userUID: BehaviorSubject<string> = new BehaviorSubject(null)
    constructor(private auth: AngularFireAuth, private router: Router) {
        auth.authState.subscribe(data => {
            this.userUID.next(data?.uid)
            if (data.uid) {
                localStorage.setItem("id", data.uid)
            }
            console.log(this.userUID.value);
        })
    }
}
