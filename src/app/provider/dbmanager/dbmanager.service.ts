import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, Query } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { first, switchMap } from 'rxjs/operators';
import { UserprofileService } from '../userprofile/userprofile.service';

@Injectable({
    providedIn: 'root'
})
export class DbmanagerService {
    userCollection$: AngularFirestoreCollection
    postCollection$: AngularFirestoreCollection
    constructor(
        private afs: AngularFirestore,
        private _userProfile: UserprofileService
    ) {
        this.userCollection$ = afs.collection("users")
        this.postCollection$ = afs.collection("posts", ref => ref.orderBy("id", "desc"))

        //this.getUserPosts()
    }

    getUserCollection() {
        return this.userCollection$.valueChanges()
    }

    storeNewUser(obj, id) {
        return this.userCollection$.doc(id).set(obj)
    }

    get currentUserCollection$() {
        return this.userCollection$.doc(localStorage.getItem('id'))
    }

    get userData() {
        //const doc = this.docExists(localStorage.getItem("id"))
        return this.userCollection$.doc(localStorage.getItem('id')).valueChanges()
        //this.userCollection$.doc(this._userProfile.userUID.value).valueChanges()
    }
    get getPosts() {
        return this.postCollection$.valueChanges()
    }


    getUserPosts(): Observable<any> {
        return this._userProfile.userUID.pipe(
            switchMap(id => {
                return this.afs.collection("posts", ref => {
                    let query: Query = ref
                    if (id) {
                        query = query.where('userId', '==', id).orderBy("id", "desc")
                    }
                    return query
                }).valueChanges()
            })
        )
    }

    async updateProfileData(obj) {
        const doc = await this.docExists(localStorage.getItem("id"))
        if (!doc) {
            return this.currentUserCollection$.update(obj)
        } else {
            return this.currentUserCollection$.set(obj)
        }
    }
    uploaduserPostData(obj) {
        return this.postCollection$.doc(obj.id).set(obj)
    }

    deletePostById(id) {
        return this.postCollection$.doc(id).delete()
    }

    docExists(id) {
        return this.afs.collection("posts").doc(id).valueChanges().pipe(first()).toPromise()
    }

    updateLatLng(obj) {
        return this.currentUserCollection$.update(obj)
    }
    getPosition() {
        let options = {
            enableHighAccuracy: true,
            timeout: 10000,
            maximumAge: 0
        };

        let regi = navigator.geolocation.watchPosition(resp => {
            navigator.geolocation.clearWatch(regi);
            return { latitude: resp.coords.latitude, longitude: resp.coords.longitude }
        }, null, options)
    }

}
