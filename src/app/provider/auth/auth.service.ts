import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private auth: AngularFireAuth, private router: Router) { }

    LoginWithGoogle() {
        return this.auth.signInWithPopup(new firebase.default.auth.GoogleAuthProvider())
    }

    LoginWithEmail(obj) {
        return this.auth.signInWithEmailAndPassword(obj.email, obj.password)
    }

    Registation(obj) {
        return this.auth.createUserWithEmailAndPassword(obj.email, obj.password)
    }

    passwordReset(email) {
        return this.auth.sendPasswordResetEmail(email)
    }

    logout() {
        this.auth.signOut();
        localStorage.removeItem("id")
        setTimeout(() => {
            this.router.navigateByUrl("/login")
        }, 500);
    }
}
