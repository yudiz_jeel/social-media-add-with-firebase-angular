import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './page/login/login.component';
import {
    AngularFireAuthGuard,
    canActivate,
    redirectLoggedInTo,
    redirectUnauthorizedTo,
} from '@angular/fire/auth-guard';
import { HomeComponent } from './page/home/home.component';
import { RegistationComponent } from './page/registation/registation.component';
import { EditProfileComponent } from './page/edit-profile/edit-profile.component';
import { UserViewComponent } from './page/user-view/user-view.component';
import { SingleUserService } from './provider/user/single-user.service';
import { UserLocationComponent } from './page/user-location/user-location.component';

const redirectLoggedInToItems = () => redirectLoggedInTo(['/home']);
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [{
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
},
{
    path: '', component: LoginComponent,
},
{
    path: 'login', component: LoginComponent,
    ...canActivate(redirectLoggedInToItems)
},
{
    path: 'home',
    component: HomeComponent,
    canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin }
},
{
    path: "signup",
    component: RegistationComponent
},
{
    path: "edit-profile",
    component: EditProfileComponent
}, {
    path: "user/:id",
    component: UserViewComponent,
    resolve: {
        user: SingleUserService
    },
}, {
    path: "user-location",
    component: UserLocationComponent
},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
