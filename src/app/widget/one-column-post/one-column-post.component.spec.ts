import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OneColumnPostComponent } from './one-column-post.component';

describe('OneColumnPostComponent', () => {
  let component: OneColumnPostComponent;
  let fixture: ComponentFixture<OneColumnPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OneColumnPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OneColumnPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
