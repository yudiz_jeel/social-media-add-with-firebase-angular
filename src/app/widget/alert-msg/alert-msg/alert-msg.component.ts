import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-alert-msg',
    templateUrl: './alert-msg.component.html',
    styleUrls: ['./alert-msg.component.scss']
})
export class AlertMsgComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<AlertMsgComponent>,
        @Inject(MAT_DIALOG_DATA) public data) { }

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit(): void {
    }

}
