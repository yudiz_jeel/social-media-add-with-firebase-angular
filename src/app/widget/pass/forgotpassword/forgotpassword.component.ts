import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from 'src/app/provider/auth/auth.service';
import { MassageService } from 'src/app/provider/msg/massage.service';

@Component({
    selector: 'app-forgotpassword',
    templateUrl: './forgotpassword.component.html',
    styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {
    emailForm: FormGroup
    constructor(private _auth: AuthService, public dialogRef: MatDialogRef<ForgotpasswordComponent>, private _msg: MassageService, private fb: FormBuilder) { }

    ngOnInit(): void {
        this.emailForm = this.fb.group(
            { email: ["", [Validators.required, Validators.email]] }
        )

    }

    get email() {
        return this.emailForm.get("email")
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    sendResetEmail() {
        this._auth.passwordReset(this.email.value).then(data => {
            this._msg.openSnackBar("Email verification link send to " + this.email.value)
            this.onNoClick()
        })
    }

}
