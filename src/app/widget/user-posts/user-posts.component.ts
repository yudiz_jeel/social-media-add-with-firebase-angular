import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ImageViewComponent } from 'src/app/widget/image-view/image-view.component';
@Component({
    selector: 'app-user-posts',
    templateUrl: './user-posts.component.html',
    styleUrls: ['./user-posts.component.scss']
})
export class UserPostsComponent implements OnInit {
    @Input() userData: any[] = []
    constructor(public dialog: MatDialog) { }

    ngOnInit(): void {
    }
    openDialog(obj) {
        const dialogRef = this.dialog.open(ImageViewComponent, { width: '100%', data: obj });
        dialogRef.afterClosed()
    }
}
