import { AfterContentChecked, Component, Input, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-loading',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, AfterContentChecked {

    @Input() visible: boolean = false;
    constructor(private spinner: NgxSpinnerService) { }

    ngAfterContentChecked() {
        if (this.visible) {
            this.showSpinner()
        } else {
            this.spinner.hide();
        }
    }

    ngOnInit(): void {

        //this.spinner.show();
        //setTimeout(() => {
        //    this.spinner.hide();
        //}, 2000);
    }

    showSpinner() {
        this.spinner.show();
        setTimeout(() => {
            this.spinner.hide();
        }, 5000);
    }


}
