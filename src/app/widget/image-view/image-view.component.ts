import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DbmanagerService } from 'src/app/provider/dbmanager/dbmanager.service';
import { MassageService } from 'src/app/provider/msg/massage.service';
@Component({
    selector: 'app-image-view',
    templateUrl: './image-view.component.html',
    styleUrls: ['./image-view.component.scss']
})
export class ImageViewComponent implements OnInit {
    isCurrentUser: boolean = false
    constructor(public dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data,
        private _dbmanager: DbmanagerService,
        private _massage: MassageService
    ) {
        console.log(data);

    }

    ngOnInit(): void {
        if (this.data.userId == localStorage.getItem("id")) {
            this.isCurrentUser = true
        }
    }

    deletePost(id) {
        console.log(id);
        this._massage.presentAlertDilog("Are you sure, Want to delete").afterClosed().subscribe(res => {
            console.log(res);
            if (res) {
                this._dbmanager.deletePostById(id)
                this.dialog.closeAll()
            }
        })

    }

}
