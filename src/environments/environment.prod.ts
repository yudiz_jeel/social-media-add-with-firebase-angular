export const environment = {
    production: true,
    firebaseConfig: {
        apiKey: "AIzaSyCWF5HzQysighQbn-kIEs979veoM_8kQPc",
        authDomain: "fir-project-ef30c.firebaseapp.com",
        databaseURL: "https://fir-project-ef30c-default-rtdb.firebaseio.com",
        projectId: "fir-project-ef30c",
        storageBucket: "fir-project-ef30c.appspot.com",
        messagingSenderId: "905694368778",
        appId: "1:905694368778:web:0d18ba20d2543e5783bbf3",
        measurementId: "G-QCKVKP2EY9"
    }
};
