// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyCWF5HzQysighQbn-kIEs979veoM_8kQPc",
        authDomain: "fir-project-ef30c.firebaseapp.com",
        databaseURL: "https://fir-project-ef30c-default-rtdb.firebaseio.com",
        projectId: "fir-project-ef30c",
        storageBucket: "fir-project-ef30c.appspot.com",
        messagingSenderId: "905694368778",
        appId: "1:905694368778:web:0d18ba20d2543e5783bbf3",
        measurementId: "G-QCKVKP2EY9"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
